<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\Customer;
use Illuminate\Auth\Access\Response;

class PostPolicy
{
    /**
     * Determine whether the customer can view any models.
     */
    public function viewAny(Customer $customer): bool
    {
        //
    }

    /**
     * Determine whether the customer can view the model.
     */
    public function view(Customer $customer, Post $post): bool
    {
        //
    }

    /**
     * Determine whether the customer can create models.
     */
    public function create(Customer $customer): bool
    {
        //
    }

    /**
     * Determine whether the customer can update the model.
     */
    public function update(Customer $customer, Post $post)
    {
        return $customer->id === $post->customer_id;
    }

    /**
     * Determine whether the customer can delete the model.
     */
    public function delete(Customer $customer, Post $post): bool
    {
        //
    }

    /**
     * Determine whether the customer can restore the model.
     */
    public function restore(Customer $customer, Post $post): bool
    {
        //
    }

    /**
     * Determine whether the customer can permanently delete the model.
     */
    public function forceDelete(Customer $customer, Post $post): bool
    {
        //
    }
}
