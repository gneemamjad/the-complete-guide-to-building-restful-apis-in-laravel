<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Models\Customer;
use Illuminate\Http\Request;

/**
 * @OA\Info(title="My First API", version="0.1")
 */

class CustomerController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/customers",
     *     @OA\Response(response="200", description="Get Users")
     * )
     */
    public function index(Request $request)
    {
        // $customers = Customer::all();
        // $customers= Customer::paginate(10);
        $query = Customer::query();

        if ($request->filled('name')) {
            $query->where('name', 'like', '%' . $request->input('name') . '%');
        }
    
        if ($request->filled('email')) {
            $query->where('email', $request->input('email'));
        }
    
        $customers = $query->paginate(10);

        return response()->json(['customers' => $customers]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateCustomerRequest $request)
    {
        // $validatedData = $request->validate([
        //     'name' => 'required',
        //     'email' => 'required|email|unique:customers',
        //     'password' => 'required|min:6',
        // ]);
    
        $customer = Customer::create($request->all());
    
        return response()->json(['message' => 'Customer created successfully', 'customer' => $customer]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $customer= Customer::findOrFail($id);

        return response()->json(['customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:customers,email,' . $id,
            'password' => 'required|min:6',
        ]);
    
        $customer = Customer::findOrFail($id);
        $customer->update($validatedData);
    
        return response()->json(['message' => 'Customer updated successfully', 'customer' => $customer]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();
    
        return response()->json(['message' => 'Customer deleted successfully']);
    }
}
